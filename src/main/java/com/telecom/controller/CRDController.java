package com.telecom.controller;

import java.time.LocalDateTime;
import java.io.Serializable;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.format.annotation.DateTimeFormat.ISO;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import com.telecom.service.CrdService;

import io.swagger.annotations.ApiOperation;


import com.telecom.model.CrdRegistro;

@RestController
@RequestMapping("/crd")
public class CRDController {
	
	
	@Autowired
	private CrdService crdService; 
	

	
	@GetMapping("/originador/{originador}")
	@ApiOperation("Busca Originador em determinando perÃ­odo")
	public ResponseEntity<Page<CrdRegistro>> buscaOriginador(@PathVariable  @DateTimeFormat(iso = ISO.DATE_TIME) LocalDateTime inicio, @PathVariable  @DateTimeFormat(iso = ISO.DATE_TIME) LocalDateTime fim, @PathVariable String calling_number, @PageableDefault(sort = "calling_number", size = 20) Pageable pageable){
			var crd = crdService.buscaOriginador(inicio,fim,calling_number,pageable);
			return ResponseEntity.ok(crd);
	}
	
	@GetMapping("/receptor/{receptor}")
	@ApiOperation("Busca ligaÃ§Ãµes em determinando perÃ­odo")
	public ResponseEntity<Page<CrdRegistro>> buscaChamadasRecebidas(@PathVariable  @DateTimeFormat(iso = ISO.DATE_TIME) LocalDateTime inicio, @PathVariable  @DateTimeFormat(iso = ISO.DATE_TIME) LocalDateTime fim, @PathVariable String called_number, @PageableDefault(sort = "calling_number", size = 20) Pageable pageable){
			var crd = crdService.buscaChamadasRecebidas(inicio, fim, called_number, pageable);
			return ResponseEntity.ok(crd);
	}
	
	@GetMapping("/cell_in/{cell_in}")
	@ApiOperation("Busca chamadas por determinada antena de entrada")
	public ResponseEntity<Page<CrdRegistro>> buscaChamadaPorAntenaEntrada(@PathVariable String cell_in, Pageable pageable){
		 	var crd = crdService.buscaChamadaPorAntenaEntrada(cell_in, pageable);
		 	return ResponseEntity.ok(crd);
	 }
	
	@GetMapping("/cell_out/{cell_out}")
	@ApiOperation("Busca chamadas por determinada antena de saÃ­da")
	public ResponseEntity<Page<CrdRegistro>> buscaChamadaPorAntenaSaida(@PathVariable String cell_out, Pageable pageable){
		 	var crd = crdService.buscaChamadaPorAntenaSaida(cell_out, pageable);
		 	return ResponseEntity.ok(crd);
	 }
	
	

}
