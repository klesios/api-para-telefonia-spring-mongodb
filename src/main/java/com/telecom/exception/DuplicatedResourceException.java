package com.telecom.exception;



	public class DuplicatedResourceException extends RuntimeException {
		
		/**
		 * 
		 */
		private static final long serialVersionUID = -5737595707686438080L;

		public DuplicatedResourceException() {
			super("Failed to save. Resource already exists");
		}
		
		public DuplicatedResourceException(String message) {
			super(message);
		}

	

}
