package com.telecom.exception;


public class ResourceNotFoundException extends RuntimeException {
	/**
	 * 
	 */
	private static final long serialVersionUID = -4997091282336181687L;


	public ResourceNotFoundException() {
		super("Failed to search for resource with the attributes provided");
	}
		
	public ResourceNotFoundException(String message) {
		super(message);
	}

}
	
