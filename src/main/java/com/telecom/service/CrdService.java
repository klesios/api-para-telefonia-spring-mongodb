package com.telecom.service;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.data.mongodb.repository.Query;
//import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.Update;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.bson.types.ObjectId;


import com.telecom.model.CrdRegistro;
import com.telecom.repository.CrdRepository;
import com.telecom.exception.*;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@NoArgsConstructor
@Data
@Service
public class CrdService {
	
	public CrdService(@Qualifier("crd")CrdRepository crdRepository) {
		this.crdRepository = crdRepository;
	}
	
	@Autowired
	CrdRepository crdRepository;
	
	 
	public Page<CrdRegistro> buscaOriginador(LocalDateTime inicial, LocalDateTime fim, String calling_number, Pageable pageable){	
			return crdRepository.buscaOriginador(inicial, fim, calling_number, pageable);	
	}
	
	
	
	public Page<CrdRegistro> buscaChamadasRecebidas(LocalDateTime inicial, LocalDateTime fim, String called_number, Pageable pageable){
			return crdRepository.buscaChamadasRecebidas(inicial, fim, called_number, pageable);
	
			
	}
	
	 public Page<CrdRegistro> buscaChamadaPorAntenaEntrada(String call_in, Pageable pageable){
		 	return crdRepository.findByCall_in(call_in, pageable);
		 
	 }
	 
	
 public Page<CrdRegistro> buscaChamadaPorAntenaSaida(String call_out, Pageable pageable){	 
		 return crdRepository.findByCall_out(call_out, pageable);
		 
	 }
	
 public CrdRegistro save(CrdRegistro crd){
		if(crd.getId() != null || crdRepository.findById(crd.getId()).isPresent()) throw new DuplicatedResourceException();
		//crdRepository.save(crd);
		
		return crd;
	}
	
	
	public void deleteById(String id) {
		crdRepository.deleteById(id);		
	 }
	
		
}