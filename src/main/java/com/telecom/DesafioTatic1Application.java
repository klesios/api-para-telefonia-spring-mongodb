package com.telecom;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;


@ComponentScan(basePackages = {"com.telecom.controller","com.telecom.exception","com.telecom.model","com.telecom.repository","com.telecom.service;"})
@Configuration
@ComponentScan
@EnableAutoConfiguration
@SpringBootApplication
public class DesafioTatic1Application {

	public static void main(String[] args) {
		SpringApplication.run(DesafioTatic1Application.class, args);
	}
	
}


