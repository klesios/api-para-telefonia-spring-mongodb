package com.telecom.repository;

import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;
import org.springframework.stereotype.Repository;
//import org.springframework.data.querydsl.QuerydslPredicateExecutor;


import java.time.LocalDateTime;
import java.util.Optional;


import com.telecom.model.CrdRegistro;

@Qualifier("crd")
@Repository
public interface CrdRepository extends MongoRepository<CrdRepository, String> {
	
	
	@Query(value = "{start_time : {$gt : ?0, $lt : ?1}, calling_number:?2}")
	Page<CrdRegistro> buscaOriginador(LocalDateTime inicial, LocalDateTime fim, String calling_number, Pageable pageable);
	       
	@Query(value = "{start_time : {$gt : ?0, $lt : ?1}, called_number:?2}")
	Page<CrdRegistro> buscaChamadasRecebidas(LocalDateTime inicial, LocalDateTime fim, String called_number, Pageable pageable);
  
	//chamadas que passam por uma determinanda antena
     Page<CrdRegistro> findByCall_in(String call_in, Pageable pageable);
     Page<CrdRegistro> findByCall_out(String call_out, Pageable pageable);
     
    // Optional<CrdRegistro> findById(String id);
    // Optional<CrdRegistro> findByCalling_number(String calling_number,Pageable pageable);
    // Optional<CrdRegistro>save(CrdRegistro);
    
     
}
