package com.telecom.model;


import java.io.Serializable;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.List;

import javax.validation.Valid;
import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;


import com.fasterxml.jackson.annotation.JsonFormat;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;




@NoArgsConstructor
@AllArgsConstructor
@Data
@Document
public class CrdRegistro {
	
	@Id
	private String id;
	private String exchange_id; 
	private String calling_number;
	private String called_number;
	private String serial_number;
	private String call_type;
	private String call_duration;
	
	@JsonFormat(shape = JsonFormat.Shape.STRING)
	private LocalDateTime start_time; 
	
	private String imsi;
	private String switchh;
	private String call_in;
	private String call_out;
	private String tecnologia;
	private String file_name;
	private String first_lac; 
	private String last_lac;
	private String ggsn_adress;


	

}

